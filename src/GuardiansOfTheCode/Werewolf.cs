using System;

namespace GuardiansOfTheCode
{
  public class Werewolf : EnemyBase, IEnemy
  {
    public Werewolf(int health, int level, int armor)
      : base(health, level, armor)
    { }

    public void Attack(PrimaryPlayer player)
    {
      Console.WriteLine($"Werewolf attacking {player.Name}");
    }

    public void Defend(PrimaryPlayer player)
    {
      Console.WriteLine($"Werewolf defending {player.Name}");
    }
  }
}