namespace GuardiansOfTheCode
{
  public class IceStaff : IWeapon
  {
    public int Damage { get; private set; }
    public int ParalizedFor { get; private set; }
    public IceStaff(int damage, int paralizedFor)
    {
      Damage = damage;
      ParalizedFor = paralizedFor;
    }

    public void UseTo(IEnemy enemy)
    {
      enemy.Health -= Damage;
      enemy.Paralized = true;
      enemy.ParalizedFor = ParalizedFor;
    }
  }
}