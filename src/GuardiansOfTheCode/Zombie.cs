using System;

namespace GuardiansOfTheCode
{
  public class Zombie : EnemyBase, IEnemy
  {
    public Zombie(int health, int level, int armor)
      : base(health, level, armor)
    { }

    public void Attack(PrimaryPlayer player)
    {
      Console.WriteLine($"Zombie attacking {player.Name}");
    }

    public void Defend(PrimaryPlayer player)
    {
      Console.WriteLine($"Zombie defending {player.Name}");
    }
  }
}