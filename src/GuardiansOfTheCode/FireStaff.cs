namespace GuardiansOfTheCode
{
  public class FireStaff : IWeapon
  {
    public int Damage { get; private set; }
    public int FireDamage { get; private set; }
    public FireStaff(int damage, int armorDamage)
    {
      Damage = damage;
      FireDamage = armorDamage;
    }

    public void UseTo(IEnemy enemy)
    {
      enemy.Health -= Damage;
      enemy.OvertimeDamage = FireDamage;
    }
  }
}