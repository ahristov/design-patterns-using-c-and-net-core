namespace GuardiansOfTheCode
{
  public abstract class EnemyBase
  {
    private int _health;
    public int Health { get => _health; set => _health = value; }

    private readonly int _level;
    public int Level => _level;

    public int OvertimeDamage { get; set; }
    public int Armor { get; set; }
    public bool Paralized { get; set; }
    public int ParalizedFor { get; set; }

    public EnemyBase(int health, int level, int armor)
    {
      _health = health;
      _level = level;
      Armor = armor;
    }
  }
}