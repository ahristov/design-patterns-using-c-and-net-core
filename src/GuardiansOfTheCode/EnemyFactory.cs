using System.Collections.Generic;

namespace GuardiansOfTheCode
{
  public class EnemyFactory
  {
    private readonly int _areaLevel;
    private readonly Stack<Zombie> _zombiePool = new Stack<Zombie>();
    private readonly Stack<Werewolf> _werewolfPool = new Stack<Werewolf>();
    private readonly Stack<Giant> _giantPool = new Stack<Giant>();

    private void PreLoadZombies()
    {
      int count;
      int health;
      int armor;
      int level;

      if (_areaLevel < 3)
      {
        count = 15;
        health = 66;
        level = 2;
        armor = 15;
      }
      else if (_areaLevel >= 3 && _areaLevel < 10)
      {
        count = 50;
        health = 66;
        level = 5;
        armor = 15;
      }
      else
      {
        count = 200;
        health = 100;
        level = 8;
        armor = 15;
      }

      for (int i = 0; i < count; i++)
      {
        _zombiePool.Push(new Zombie(health, level, armor));
      }
    }

    private void PreLoadWerewolves()
    {
      int count;
      int health;
      int armor;
      int level;

      if (_areaLevel < 5)
      {
        count = 50;
        health = 100;
        level = 12;
        armor = 1;
      }
      else
      {
        count = 200;
        health = 100;
        level = 20;
        armor = 1;
      }

      for (int i = 0; i < count; i++)
      {
        _werewolfPool.Push(new Werewolf(health, level, armor));
      }
    }

    private void PreLoadGiants()
    {
      int count;
      int health;
      int armor;
      int level;

      if (_areaLevel < 5)
      {
        count = 5;
        health = 100;
        level = 14;
        armor = 0;
      }
      else
      {
        count = 50;
        health = 100;
        level = 32;
        armor = 0;
      }

      for (int i = 0; i < count; i++)
      {
        _giantPool.Push(new Giant(health, level, armor));
      }
    }

    public int AreaLevel { get => _areaLevel; }

    public EnemyFactory(int areaLevel)
    {
      _areaLevel = areaLevel;
      PreLoadZombies();
      PreLoadWerewolves();
      PreLoadGiants();
    }

    public Werewolf SpawnWherewolf(int areaLevel)
    {
      if (areaLevel < 5)
      {
        return new Werewolf(100, 12, 0);
      }
      else
      {
        return new Werewolf(100, 20, 0);
      }
    }

    public Giant SpawnGiant(int areaLevel)
    {
      if (areaLevel < 8)
      {
        return new Giant(100, 14, 0);
      }
      else
      {
        return new Giant(100, 32, 0);
      }
    }

    public Zombie SpawnZombie(int areaLevel)
    {
      if (areaLevel < 3)
      {
        return new Zombie(66, 2, 15);
      }
      else if (areaLevel >= 3 && areaLevel < 10)
      {
        return new Zombie(66, 5, 15);
      }
      else
      {
        return new Zombie(100, 8, 15);
      }
    }
  }
}