using System;
using System.Collections.Generic;

namespace GuardiansOfTheCode
{
  public class GameBoard
  {
    private PrimaryPlayer _player;

    public GameBoard()
    {
      _player = PrimaryPlayer.Instance;
      _player.Weapon = new Sword(12, 8);
    }

    public void PlayArea(int lvl)
    {
      if (lvl == 1)
      {
        PlayFirstLevel();
      }
    }

    private void PlayFirstLevel()
    {
      const int currentLvl = 1;
      List<IEnemy> enemies = new List<IEnemy>();
      for (int i = 0; i < 10; i++)
      {
        enemies.Add(EnemyFactory.SpawnZombie(currentLvl));
      }

      for (int i = 0; i < 3; i++)
      {
        enemies.Add(EnemyFactory.SpawnWherewolf(currentLvl));
      }

      foreach (var enemy in enemies)
      {
        while (enemy.Health > 0 && _player.Health > 0)
        {
          _player.Weapon.UseTo(enemy);
          enemy.Attack(_player);
        }
        if (enemy.Health <= 0) { Console.WriteLine("Enemy is dead"); }
        if (_player.Health <= 0) { Console.WriteLine("Player is dead"); }
      }
    }
  }
}