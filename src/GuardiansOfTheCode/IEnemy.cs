namespace GuardiansOfTheCode
{
  public interface IEnemy
  {
    int Health { get; set; }
    int Level { get; }
    int OvertimeDamage { get; set; }
    int Armor { get; set; }
    bool Paralized { get; set; }
    int ParalizedFor { get; set; }
    void Attack(PrimaryPlayer player);
    void Defend(PrimaryPlayer player);
  }
}