namespace GuardiansOfTheCode
{
  public class Sword : IWeapon
  {
    public int Damage { get; private set; }
    public int ArmorDamage { get; private set; }
    public Sword(int damage, int armorDamage)
    {
      Damage = damage;
      ArmorDamage = armorDamage;
    }

    public void UseTo(IEnemy enemy)
    {
      enemy.Health -= Damage;
      enemy.Armor -= ArmorDamage;
    }
  }
}