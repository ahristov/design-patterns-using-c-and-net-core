using System;

namespace GuardiansOfTheCode
{
  public class Giant : EnemyBase, IEnemy
  {
    public Giant(int health, int level, int armor)
      : base(health, level, armor)
    { }

    public void Attack(PrimaryPlayer player)
    {
      Console.WriteLine($"Giant attacking {player.Name}");
    }

    public void Defend(PrimaryPlayer player)
    {
      Console.WriteLine($"Giant defending {player.Name}");
    }
  }
}