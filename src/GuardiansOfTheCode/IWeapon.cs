namespace GuardiansOfTheCode
{
  public interface IWeapon
  {
    int Damage { get; }
    void UseTo(IEnemy enemy);
  }
}