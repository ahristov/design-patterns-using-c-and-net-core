# Design Patterns Using C# and .NET Core Video

Code and notes from studying [Design Patterns Using C# and .NET Core Video](https://www.packtpub.com/application-development/design-patterns-using-c-and-net-core-video)

## SOLID Principles

SOLID principles are:

- S: Single responsibility

The class should be responsible for managing single part of functionality.

- O: Open/closed principle

The class should be open to extensions, but closed for modifications.  
You should not add stuff to completed classes.

- L: Liskov substitution

You should be able to use a subclass in place of its parent class.

- I: Interface segregation

A class should not depend on methods that it does not need to implement.  
Split the interfaces if your class implements methods that does not need.

- D: Dependency injection

Your classes should depend on abstraction instead of concrete implementations.

## Test Doubles

Test doubles are:

- Fake: Objects with working implementation, but not same as the production version.
- Stub: Objects that hold predefined data.
- Mock: Objects that register calls they receive.

## Lose Coupling

As soon as interfaces match, we do not care how the actual components internally work.

Benefits:

- Easier to work with large projects
- Swap implementations
- Testability
- Components can be developed independently

## Design Pattern Groups

They are 3 groups of design patterns:

- Creational: how we instantiate objects and how many instances we are allowed to create.
- Structural: composing classes and objects.
- Behavioral: determine ways of application flow.

Each of the groups of patterns is implemented in the sample code. The code is a simulation of a card game called "Guardians of the Code".

## Creational Patterns

Creational patterns are about how we instantiate objects and how many instances we are allowed to create.

### Singleton Pattern

Definition:

    You can only have a single instance of a specific class
    throughout the entire application.

We avoid to use the keyword `new`.  
We are using method that returns single instance from a _shared state_.  
Use to instantiate objects with long initialization.  
Represent unique items that are easy accessible from everywhere.

Key implementation points:

- Private constructor
- Read-only instance
- Static instance
- Static accessor
- The class should be sealed
- The instance should be initialized in a static constructor

The static constructor runs only once in the whole application domain.
Because of that is provides the following benefits:

- Lazy Loading: the static constructor will only run when needed
- Thread Safety: multiple threads always will access one instance

Anti-patterns:

- The singleton violated the Dependency Injection Principle.
- It creates an instance of itself.

Implementation:

In the game singleton is used to get the player's instance of the main player of the game. See: `PrimaryPlayer` class.

### Factory Pattern

Definition:

    A combination of the Single Responsibility and Open/Closed Principles.

Implementation:

We have three types of enemies: Zombies, Werewolves and Giants.
In the future we want to be able to add more types of enemies, like a Troll for example.
We need an easy way to add more enemy types in the future.

We have an interface `IEnemy`.  
We also have a `GameBoard`. It is responsible to create our player, and the enemies to face at each level.  
Following the Single Responsibility Principle, the game board does not need to know how to instantiate enemies, or what enemies to create.  
Also, enemies have different health levels at different stage of the game. Again, this is not a concern for the `GameBoard` class.  
The `GameBoard` holds data like primary player level etc.  
We create `EnemyFactory` that knows how to instantiate the `IEnemy` objects.

### Object Pool Pattern

Definition:

    The Object Pool isa collection of pre-initialized objects.

Every time we need such we take one from the pool and return is back to the pool. That way we avoid constantly initializing and destroying objects.

Anti-patterns:

- Maintaining an object pool is a headache.
- However it is still used in game development for example.

Implementation:

We convert all the `static` class methods of the `EnemyFactory` class to object methods. The factory will then need to hold some state.

## Structural Patterns

Structural patterns are about composing classes and objects. They offer efficient way to solve composition challenges.

## Behavioral Patterns

They determine ways of application flow - how objects communicate with each other; how they interact with each other.
